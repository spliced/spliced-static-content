<?php

use Spliced\Plugin\Front_Matter;

// require_once '../plugin.php';
require_once '../Plugin/Front_Matter.php';


class SplicedFrontMatterTests extends WP_UnitTestCase {
	var $docWithFrontMatter;
	var $docWithoutFrontMatter;

	public function setUp() {
		parent::setUp();

		$this->docWithFrontMatter = <<<EOF
---
Some front matter
---
Some main document content
EOF;

		$this->docWithoutFrontMatter = <<<EOF
Some main document content
EOF;

	}

	public function testParseWithFrontMatter() {
		$fm = new Front_Matter( $this->docWithFrontMatter );

		$this->assertEquals( $fm->get_content(), 'Some main document content', 'Main document content does not match' );
		$this->assertEquals( $fm->get_matter(), 'Some front matter', 'Front matter does not match' );
	}

	public function testParseWithoutFrontMatter() {
		$fm = new Front_Matter( $this->docWithoutFrontMatter );

		$this->assertEquals( $fm->get_content(), 'Some main document content', 'Main document content does not match' );
		$this->assertFalse( $fm->get_matter(), 'Extreneous front matter found' );
	}
}
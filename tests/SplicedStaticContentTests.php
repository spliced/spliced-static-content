<?php
use Spliced\Plugin\Static_Content as Plugin;

// require_once '../plugin.php';
require_once '../../../mu-plugins/bisso-hooker.php';


class SplicedStaticContentTests extends WP_UnitTestCase {
	protected $plugin;
	protected $contentFixturesPath;

	public function setUp() {
		parent::setUp();
		$this->contentFixturesPath = __DIR__ . '/fixtures/site';
		require_once '../plugin.php';

		Plugin::activation();
	}

	public function tearDown() {
		parent::tearDown();

		$GLOBALS['wp_styles'] = new stdClass();
		$GLOBALS['wp_scripts'] = new stdClass();
	}

	public function testInitialization() {
		$this->assertTrue( class_exists( '\Spliced\Plugin\Static_Content' ), "Plugin class doesn't exist" );
		$this->assertInstanceOf( 'Bisso_Hooker', Plugin::$_hooker, 'Hooker class not found in plugin' );
	}

	public function testGetWidgetOptionsNotFound() {
		$this->assertFalse( Plugin::get_widget_options() );
	}

	public function testGetWidgetOptions() {
		Plugin::$content_path = $this->contentFixturesPath;

		$this->assertTrue( is_array( Plugin::get_widget_options() ) );
	}

	public function testPermalinksEnabled() {
		$post_name = 'testpost1';

		$post = array(
			'post_status'  => 'publish',
			'post_content' => '',
			'post_title'   => $post_name,
			'post_type'    => 'page',
		);

		$id = wp_insert_post( $post );
		$out = get_post( $id );

		$this->assertEquals( get_permalink( $id ), site_url( $post_name ) );
	}

	public function testGetPostContent() {
		Plugin::$content_path = $this->contentFixturesPath;

		$post_name = 'testpost1';

		$post = array(
			'post_status'  => 'publish',
			'post_content' => '',
			'post_title'   => $post_name,
			'post_type'    => 'page',
		);

		// insert a post and make sure the ID is ok
		$id = wp_insert_post( $post );
		$this->assertTrue( is_numeric( $id ) );
		$this->assertTrue( $id > 0 );

		// fetch the post and make sure it matches
		$out = get_post( $id );

		$content = Plugin::get_post_content( $out );
		$fixture = file_get_contents( "{$this->contentFixturesPath}/posts/{$post_name}.html" );

		$this->assertEquals( $content, $fixture );
	}

	public function testLocatePostContentTemplate() {
		Plugin::$content_path = $this->contentFixturesPath;

		$parent_post_name = 'parent-page';
		$post = array(
			'post_status'  => 'publish',
			'post_content' => '',
			'post_title'   => $parent_post_name,
			'post_type'    => 'page',
		);

		$parent_post_id = wp_insert_post( $post );

		$child_post_name = 'child-page';
		$post = array(
			'post_status'  => 'publish',
			'post_content' => '',
			'post_title'   => $child_post_name,
			'post_type'    => 'page',
			'post_parent'  => $parent_post_id,
		);

		$child_post_id = wp_insert_post( $post );

		$template_path = Plugin::locate_post_content_template( $child_post_id );

		$this->assertEquals( $template_path, "{$this->contentFixturesPath}/posts/{$parent_post_name}/{$child_post_name}.html" );
	}

	/**
	 * Can we filter the post content to add our static content
	 * @depends testPermalinksEnabled
	 */
	public function testFilterTheContent() {
		Plugin::$content_path = $this->contentFixturesPath;
		$post_name = 'testpost1';

		$post = array(
			'post_status'  => 'publish',
			'post_content' => '',
			'post_title'   => $post_name,
			'post_type'    => 'page',
		);

		// insert a post and make sure the ID is ok
		$id = wp_insert_post( $post );
		$post = get_post( $id );

		$this->go_to( site_url( get_permalink( $post->ID ) ) );
		$GLOBALS['post'] = $post;

		$this->assertEquals( 'http://wordpress.local/' . $post_name, get_permalink( $post->ID ) );

		ob_start();
		the_content();
		$content = ob_get_clean();

		$this->assertEquals( $this->contentFixturesPath . '/posts/' . $post_name . '.html', Plugin::locate_post_content_template( $post->ID ) );
		$this->assertEquals( $content, file_get_contents( Plugin::locate_post_content_template( $post->ID ) ) );

//         $this->markTestIncomplete(
//           'This test has not been implemented yet.'
//         );
	}

	public function testLocatePostContentTemplateFormats() {
		Plugin::$content_path = $this->contentFixturesPath;
		Plugin::$template_extensions = array( 'md', 'mustache', 'html' );

		$post_name = 'testpost-formats';

		$post = array(
			'post_status'  => 'publish',
			'post_content' => '',
			'post_title'   => $post_name,
			'post_type'    => 'page',
		);

		// insert a post and make sure the ID is ok
		$id = wp_insert_post( $post );
		$post = get_post( $id );

		$templates = array(
			$this->contentFixturesPath . '/posts/' . $post_name . '.md',
			$this->contentFixturesPath . '/posts/' . $post_name . '.mustache',
			$this->contentFixturesPath . '/posts/' . $post_name . '.html',
		);

		$this->assertEquals( $templates, Plugin::locate_post_content_templates( $post->ID ) );
	}

	/**
	 * @dataProvider templateFormats
	 */
	public function testGetTemplateFormats( $template, $format ) {
		$this->assertEquals( $format, Plugin::get_template_format( $template ) );
	}

	/**
	 * Maps example paths to template formats
	 * @return array
	 */
	public function templateFormats() {
		return array(
			array( '/some/path/example.md', 'markdown' ),
			array( '/some/path/example.markdown', 'markdown' ),
			array( '/some/path/example.mustache', 'mustache' ),
			array( '/some/path/example.html', 'html' ),
			array( '/some/path/example.txt', 'text' ),
			array( '/some/path/example', '' ),
		);
	}

	public function testPostContentMarkdown() {
		Plugin::$content_path = $this->contentFixturesPath;
		Plugin::$template_extensions = array( 'md', 'mustache', 'html' );

		$post_name = 'testpost-formats';

		$post_array = array(
			'post_status'  => 'publish',
			'post_content' => '',
			'post_title'   => $post_name,
			'post_type'    => 'page',
		);

		$GLOBALS['post'] = $post = $this->postFactory( $post_array );

		$this->assertEquals( 'http://wordpress.local/' . $post_name, get_permalink( $post->ID ) );

		$content       = $this->getPostContent();
		$template      = Plugin::locate_post_content_template( $post->ID );
		$template_html = $this->contentFixturesPath . '/posts/' . $post_name . '.html';

		$this->assertEquals( $this->contentFixturesPath . '/posts/' . $post_name . '.md', $template );
		$this->assertEquals( 'markdown', Plugin::get_template_format( $template ) );
		$this->assertEquals( $content, file_get_contents( $template_html ) );
	}

	public function testPostWithFrontMatter() {
		Plugin::$content_path = $this->contentFixturesPath;

		$post_name = 'testpost-withfrontmatter';
		$post_array = array(
			'post_status'  => 'publish',
			'post_content' => '',
			'post_title'   => $post_name,
			'post_type'    => 'page',
		);

		$GLOBALS['post'] = $post = $this->postFactory( $post_array );

		$content = $this->getPostContent();
		$rawFrontMatter = Plugin::get_post_front_matter( $post, true );
		$frontMatter = Plugin::get_post_front_matter( $post );

		$expectedFrontMatter = array(
			'some' => 'frontmatter',
			'post_title' => 'Test Post Title',
		);

		$expectedRawFrontMatter = <<<EOF
some: frontmatter
post_title: Test Post Title
EOF;

		$expectedContent = <<<EOF
<h3>A header</h3>

<p>Some content in a paragraph</p>
EOF;

		// Test parsing of front matter
		$this->assertEquals( $expectedContent, $content, 'Unexpected post content' );
		$this->assertEquals( $expectedRawFrontMatter, $rawFrontMatter, 'Unexpected raw front matter' );
		$this->assertEquals( $expectedFrontMatter, $frontMatter, 'Unexpected YAML parsed front matter' );

		// Test post title override from front matter
		$this->assertNotEquals( 'Test Post Title', get_the_title( 1 ) );
		$this->assertEquals( 'Test Post Title', get_the_title( $post->ID ) );
	}

	public function testGetCachedFrontMatter() {
		$post_name = 'testpost-withfrontmatter';

		$post = $this->postFactory(
			array(
				'post_title'   => $post_name,
				'post_type'    => 'page',
			)
		);

		$expectedRawFrontMatter = <<<EOF
some: frontmatter
post_title: Test Post Title
EOF;

		$frontMatter_first = Plugin::get_post_front_matter( $post );

		// Raw matter is never cached
		$frontMatter_raw = Plugin::get_post_front_matter( $post, true );
		$frontMatter_cached = Plugin::get_post_front_matter( $post );

		$this->assertEquals( $expectedRawFrontMatter, $frontMatter_raw );
		$this->assertEquals( $frontMatter_first, $frontMatter_cached );
	}

	public function testPostWithFrontMatterMeta() {
		Plugin::$content_path = $this->contentFixturesPath;

		$post_name = 'testpost-withfrontmattermeta';
		$post = $this->postFactory(
			array(
				'post_title'   => $post_name,
				'post_type'    => 'page',
			)
		);

		$meta_value = get_post_meta( $post->ID, 'a_meta_name', true );
		$this->assertEquals( $meta_value, 'a meta value' );

		$meta_value = get_post_meta( $post->ID, 'another_meta', true );
		$this->assertEquals(
			$meta_value,
			array(
				'with' => 'more',
				'complex' => 'data',
			)
		);
	}

	public function testDoNotGetFrontMatter() {
		// Post 1 is 'hello world' – it has not front matter
		$matter = Plugin::get_post_front_matter( get_post( 1 ) );

		$this->assertEmpty( $matter );
	}

	private function postFactory( array $config ) {
		$defaults = array(
			'post_title'   => 'post_title',
			'post_status'  => 'publish',
			'post_content' => '',
		);

		$id = wp_insert_post( array_merge( $defaults, $config ) );
		return get_post( $id );
	}

	private function getPostContent() {
		ob_start();
		the_content();
		$content = ob_get_clean();

		return $content;
	}

}
<?php
/*
Plugin Name: Spliced Static Content
Plugin URI: http://spliced.co
Description: Allows site builder to store content in static files rather than the database
Version: 0.1
Author: Dan Bissonnet, Spliced Digital
Author URI: http://spliced.co/
*/

/**
 * Copyright (c) 2012 Your Name. All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * **********************************************************************
 */
namespace {
	$loader = include_once __DIR__ . '/vendor/autoload.php';

	$loader->addClassMap(
		array(
			'Spliced\Plugin\Static_Content' => __DIR__ . '/Plugin/Static_Content.php',
			'Spliced\Plugin\Front_Matter' => __DIR__ . '/Plugin/Front_Matter.php',
		)
	);
}

namespace Spliced\Plugin {
	/**
	 * Bootstrap or die
	 */
	try {
		if ( class_exists( 'Bisso_Hooker' ) ) {
			Static_Content::bootstrap( new \Bisso_Hooker );
		} else {
			throw new \Exception( 'Class Bisso_Hooker not found. Check that the plugin is installed.', 1 );
		}
	} catch ( \Exception $e ) {
		wp_die( $e->getMessage(), $title = 'Theme Exception' );
	}
}

<?php
namespace Spliced\Plugin;

use \Michelf\Markdown;
use \Michelf\MarkdownExtra;
use \Spliced\Plugin\Front_Matter;
use \Symfony\Component\Yaml\Yaml;

/**
 * Class Static_Content
 */
class Static_Content {
	static $_hooker;
	static $settings = array();
	static $content_path;
	static $template_extensions = array();

	function bootstrap( $hooker = null ) {
		if ( $hooker ) {
			if ( !method_exists( $hooker, 'hook' ) )
				throw new \BadMethodCallException( 'Class ' . get_class( $hooker ) . ' has no hook() method.', 1 );

			self::$_hooker = $hooker;
			self::$_hooker->hook( __CLASS__, 'spliced-static-content' );
		} else {
			throw new \BadMethodCallException( 'Hooking class for ' . __CLASS__ . ' not specified.' , 1 );
		}

		register_activation_hook( __FILE__, array( __CLASS__, 'activation' ) );

		self::$content_path = WP_CONTENT_DIR . '/site';
		self::$template_extensions = array( 'md', 'mustache', 'html' );
	}

	function activation() {
		global $wp_rewrite;

		$permalink_structure = get_option( 'permalink_structure' );

		if ( empty( $permalink_structure ) ) {
			$wp_rewrite->set_permalink_structure( '/%postname%' );
			$wp_rewrite->flush_rules();
		}
	}

	function filter_wp_get_nav_menus( $nav_menus, $args ) {
		$files = glob( self::$content_path . '/menus/*/menu.{php}', GLOB_BRACE );

		foreach ( $files as $file ) {
			include_once $file;
			$menu = (object) $menu;

			wp_cache_add( $menu->term_id, $menu, 'nav_menu' );

			$nav_menus[] = (object) $menu;
		}

		return $nav_menus;
	}

	function filter_wp_get_nav_menu_items( $menu_items, $menu, $args ) {

		$files = glob( self::$content_path . "/menus/{$menu->slug}/menu.{php}", GLOB_BRACE );

		foreach ( $files as $file ) {
			include_once $file;

			array_push( $menu_items, $items );
		}

		return $menu_items;
	}

	function filter_get_term( $term, $taxonomy ) {
		// var_dump($term);
		return $term;
	}

	function filter_option_sidebars_widgets( $sections ) {
		return $sections;
	}

	function action_updated_option( $option_name, $oldvalue, $_newvalue ) {
		if ( 'sidebars_widgets' === $option_name || preg_match( '/^widget_/', $option_name ) ) {
			file_put_contents(
				self::$content_path . '/widgets/widgets.json',
				self::pretty_json( json_encode( self::build_widget_data() ) )
			);
		}
	}

	function build_widget_data() {
		$sidebars = get_option( 'sidebars_widgets' );

		unset( $sidebars['array_version'] );

		foreach ( $sidebars as $area => &$widgets ) {
			if ( !empty($widgets) ) {
			 	foreach ( $widgets as $key => &$widget ) {
			 		if ( !is_string( $widget ) ) continue;

					preg_match_all( '/(.*)-([0-9]+)/', $widget, $matches );

					$widget_settings = get_option( "widget_{$matches[1][0]}" );
					$widget_settings = $widget_settings[$matches[2][0]];
					$widget_settings['_widget'] = $matches[1][0];
					$widget_settings['_widget_number'] = (int) $matches[2][0];
					$widgets[$matches[0][0]] = $widget_settings;

					unset( $widgets[$key] );
				}
			}
		}

		return $sidebars;
	}

	function get_widget_options() {
		$data_path = self::$content_path . '/widgets/widgets.json';

		if ( !file_exists( $data_path ) )
			return false;

		$raw_data       = (array) json_decode( file_get_contents( $data_path ), true );
		$widget_options = array();

		$sidebars_widgets = array_map(
			function( $item ) use ( &$widget_options ) {
				foreach ( $item as $widget_id => $widget_data ) {
					$option_name   = 'widget_' . $widget_data['_widget'];
					$widget_number = (int)$widget_data['_widget_number'];
					unset($widget_data['_widget_number']);
					unset($widget_data['_widget']);
					$widget_options[$option_name][$widget_number] = $widget_data;
					$widget_options[$option_name]['_multiwidget'] = true;
				}
				return array_keys( $item );
			},
			$raw_data
		);

		return array( 'sidebars_widgets' => $sidebars_widgets ) + $widget_options;
	}

	function action_sidebar_admin_setup() {
		$widget_data = self::get_widget_options();

		foreach ( $widget_data as $option => $value ) {
			update_option( $option, $value );
		}
	}

	function filter_the_content( $content ) {
		$post = get_post();
		$template = self::locate_post_content_template( $post->ID );

		if ( file_exists( $template ) ) {
			return self::get_post_content( $post );
		}

		return $content;
	}

	public function get_post_content( \WP_Post $post ) {
		$template     = self::locate_post_content_template( $post->ID );
		$format       = self::get_template_format( $template );
		$content_raw  = file_get_contents( $template );
		$front_matter = new Front_Matter( file_get_contents( $template ) );


		if ( $format == 'markdown' ) {
			$content = trim( MarkdownExtra::defaultTransform( $front_matter->get_content() ) );
		} else {
			$content = $front_matter->get_content();
		}

		return do_shortcode( $content );
	}

	public function get_post_front_matter( \WP_Post $post, $raw = false ) {
		static $cache = array();

		if ( !$raw && !empty( $cache[$post->ID] ) ) {
			return $cache[$post->ID];
		}

		$matter = '';
		$raw_matter = '';

		if ( $template = self::locate_post_content_template( $post->ID ) ) {
			$front_matter = new Front_Matter( file_get_contents( $template ) );
			$matter       = $front_matter->get_matter();
			$raw_matter = $matter;

			if ( !empty( $matter ) && !$raw ) {
				$matter = Yaml::parse( $raw_matter );
				$cache[$post->ID] = $matter;
			}
		}

		return $matter;
	}

	public function filter_the_title( $title, $post_id ) {
		$matter = self::get_post_front_matter( get_post( $post_id ) );

		if ( isset( $matter['post_title'] ) )
			return $matter['post_title'];

		return $title;
	}

	function filter_get_post_metadata( $value, $post_id, $meta_key, $single ) {
		$matter = self::get_post_front_matter( get_post( $post_id ) );
		$meta = $matter['meta'];

		if ( !empty( $meta ) && isset( $meta[$meta_key] ) ) {
			$value = array( $meta[$meta_key] );
		}

		return $value;
	}


	public function get_template_format( $template ) {
		switch( pathinfo( $template, PATHINFO_EXTENSION ) ) {
			case 'markdown':
			case 'md':
				$format = 'markdown';
				break;
			case 'mustache':
				$format = 'mustache';
				break;
			case 'html':
				$format = 'html';
				break;
			case 'txt':
				$format = 'text';
				break;
			default:
				$format = '';
				break;
		}

		return $format;
	}

	public function locate_post_content_template( $post_id ) {
		return array_shift( self::locate_post_content_templates( $post_id ) );
	}

	public function locate_post_content_templates( $post_id ) {
		$post_permalink          = get_permalink( $post_id );
		$post_permalink_relative = str_replace( home_url( '/' ), '',  $post_permalink );
		$extensions              = '.{' . implode( ',', self::$template_extensions ) . '}';

		return glob( self::$content_path . '/posts/' . untrailingslashit( $post_permalink_relative ) . $extensions, GLOB_BRACE );
	}

	/**
	 * Indents a flat JSON string to make it more human-readable.
	 *
	 * @param string $json The original JSON string to process.
	 *
	 * @return string Indented version of the original JSON string.
	 */
	function pretty_json( $json ) {

	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen( $json );
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;

	    for ( $i = 0; $i <= $strLen; $i++ ) {
	        // Grab the next character in the string.
	        $char = substr( $json, $i, 1 );

	        // Are we inside a quoted string?
	        if ( $char == '"' && $prevChar != '\\' ) {
	            $outOfQuotes = !$outOfQuotes;

	        // If this character is the end of an element,
	        // output a new line and indent the next line.
	        } else if ( ( $char == '}' || $char == ']') && $outOfQuotes ) {
	            $result .= $newLine;
	            $pos --;
	            for ( $j = 0; $j < $pos; $j++ ) {
	                $result .= $indentStr;
	            }
	        }

	        // Add the character to the result string.
	        $result .= $char;

	        // If the last character was the beginning of an element,
	        // output a new line and indent the next line.
	        if ( ( $char == ',' || $char == '{' || $char == '[') && $outOfQuotes ) {
	            $result .= $newLine;
	            if ( $char == '{' || $char == '[' ) {
	                $pos ++;
	            }

	            for ( $j = 0; $j < $pos; $j++ ) {
	                $result .= $indentStr;
	            }
	        }

	        $prevChar = $char;
	    }

	    return $result;
	}
}
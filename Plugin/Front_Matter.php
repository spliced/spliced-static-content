<?php

namespace Spliced\Plugin;

class Front_Matter {
	static $separator = "---\n";
	public $matter;
	public $content;

	public function __construct( $content ) {
		$this->parse( $content );
	}

	public function parse( $content ) {
		$content_parts = explode( self::$separator, $content, 3 );

		if ( count( $content_parts ) == 3 ) {
	 		$this->matter = trim( $content_parts[1] );
			$this->content = trim( $content_parts[2] );
		}

		$this->raw_content = $content;

		return $content_parts;
	}

	public function get_content() {
		return isset( $this->content ) ? $this->content : $this->raw_content;
	}

	public function get_matter() {
		return isset( $this->matter ) ? $this->matter : false;
	}
}